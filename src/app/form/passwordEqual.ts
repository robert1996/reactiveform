import {FormControl} from '@angular/forms';

export class PasswordEqual {
  static pswEqual(control:FormControl):{[key:string]:boolean} {

    let password = control.get('psw');
    let confirm = control.get('cpsw');

    if (password && confirm) {
      if (password.value === confirm.value) {
        return {'pswEqual': true};
      } else {
        return null;
      }
    }return null
  }
}
