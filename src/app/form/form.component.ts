import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, FormArray} from '@angular/forms';
import {User} from './form.interface';
import {PasswordEqual} from './passwordEqual';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {

  form:FormGroup;

  constructor() {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(12), Validators.pattern("^[a-zA-Z]*$")]),
      age: new FormControl('', [Validators.required, Validators.pattern("^[0-9]+$")]),
      email: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(20), Validators.pattern("[a-zA-Z_]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}")]),
      password: new FormGroup({
        psw: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(15), Validators.pattern("^[a-zA-Z]*$")]),
        cpsw: new FormControl('', [Validators.required, PasswordEqual.pswEqual, Validators.minLength(8), Validators.maxLength(15), Validators.pattern("^[a-zA-Z]*$")]),
      }),
      country: new FormControl('', [Validators.required]),
      phones: new FormArray([
        new FormControl("374", [Validators.required, Validators.pattern("^[0-9]+$")])
      ]),
      language: new FormArray([
        new FormControl(''),
        new FormControl(''),
        new FormControl(''),
      ]),
      gender: new FormControl('')
    })
  }

  addPhone() {
    (<FormArray>this.form.controls["phones"]).push(new FormControl("374", [Validators.required, Validators.pattern("^[0-9]+$")]));
  }

  onSubmit(value:User, valid:boolean) {
    console.log(value, valid);
  }
}
