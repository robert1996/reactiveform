export class User {
  name:string;
  age:string;
  email:string;
  password:{
    psw:string;
    cpsw:string
  };
  country:string;
  language: boolean[];
  gender: string;
}
