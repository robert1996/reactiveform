import { Angular2ReactiveFormPage } from './app.po';

describe('angular2-reactive-form App', function() {
  let page: Angular2ReactiveFormPage;

  beforeEach(() => {
    page = new Angular2ReactiveFormPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
